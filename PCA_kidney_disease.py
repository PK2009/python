import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import datetime
from mpl_toolkits.mplot3d import Axes3D
from plyfile import PlyData, PlyElement
import assignment2_helper as helper
from sklearn.decomposition import PCA
# Look pretty...
matplotlib.style.use('ggplot')


# Do * NOT * alter this line, until instructed!
scaleFeatures = False


# TODO: Load up the dataset and remove any and all
# Rows that have a nan. You should be a pro at this
# by now ;-)
#
# .. your code here ..

df = pd.read_csv('/Users/prathmesh/Desktop/ML/PCA/Datasets/kidney_disease.csv')
df = df.drop(labels=['id'], axis=1)
df = df.dropna(axis=0, how='any')

# Create some color coded labels; the actual label feature
# will be removed prior to executing PCA, since it's unsupervised.
# You're only labeling by color so you can see the effects of PCA
labels = ['red' if i=='ckd' else 'green' for i in df.classification]


# TODO: Use an indexer to select only the following columns:
#       ['bgr','wc','rc']
#
# .. your code here ..
s = pd.DataFrame()
s1 = (df[['bgr','rc','wc']])
#s = pd.DataFrame([list(range(3))], columns=[s1.bgr])
s2 = pd.to_numeric(s1.bgr, errors='coerce')
s3 = pd.to_numeric(s1.rc, errors='coerce')
s4 = pd.to_numeric(s1.wc, errors='coerce')
s['bgr']= s2
s['rc']= s3
s['wc']= s4
#s = pd.DataFrame([s3,s2,s1])



# TODO: Print out and check your dataframe's dtypes. You'll probably
# want to call 'exit()' after you print it out so you can stop the
# program's execution.
#
# You can either take a look at the dataset webpage in the attribute info
# section: https://archive.ics.uci.edu/ml/datasets/Chronic_Kidney_Disease
# or you can actually peek through the dataframe by printing a few rows.
# What kind of data type should these three columns be? If Pandas didn't
# properly detect and convert them to that data type for you, then use
# an appropriate command to coerce these features into the right type.
#
# .. your code here ..

print(np.var(s))
print(s.describe())


# TODO: PCA Operates based on variance. The variable with the greatest
# variance will dominate. Go ahead and peek into your data using a
# command that will check the variance of every feature in your dataset.
# Print out the results. Also print out the results of running .describe
# on your dataset.
#
# Hint: If you don't see all three variables: 'bgr','wc' and 'rc', then
# you probably didn't complete the previous step properly.
#
# .. your code here ..











# TODO: This method assumes your dataframe is called df. If it isn't,
# make the appropriate changes. Don't alter the code in scaleFeatures()
# just yet though!
#
# .. your code adjustment here ..
if scaleFeatures: s = helper.scaleFeatures(s)

'''
t1 = datetime.datetime.now()
pca = do_PCA(s)
time_delta = datetime.datetime.now() - t1
if not pca is None:
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_title('PCA, build time: ' + str(time_delta))
  ax.scatter(pca[:,0], pca[:,1], c='blue', marker='.', alpha=0.75)
'''



# TODO: Run PCA on your dataset and reduce it to 2 components
# Ensure your PCA instance is saved in a variable called 'pca',
# and that the results of your transformation are saved in 'T'.
#
# .. your code here ..


pca = PCA(n_components=2)
pca.fit(s)
PCA(copy=True, n_components=2, whiten=False)
T = pca.transform(s)
m = T.shape





# Plot the transformed data as a scatter plot. Recall that transforming
# the data will result in a NumPy NDArray. You can either use MatPlotLib
# to graph it directly, or you can convert it to DataFrame and have pandas
# do it for you.
#
# Since we've already demonstrated how to plot directly with MatPlotLib in
# Module4/assignment1.py, this time we'll convert to a Pandas Dataframe.
#
# Since we transformed via PCA, we no longer have column names. We know we
# are in P.C. space, so we'll just define the coordinates accordingly:

ax = helper.drawVectors(T, pca.components_, s.columns.values, plt, scaleFeatures)

T.columns = ['component1', 'component2']
T.plot.scatter(x='component1', y='component2', marker='o', c=labels, alpha=0.75, ax=ax)
plt.show()


