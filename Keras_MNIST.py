import keras
import numpy as np
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K


# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# input image dimensions
img_rows, img_cols = 28, 28
# number of classes (digits) to predict
num_classes = 10

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
#model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])


batch_size = 128
epochs = 1

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))






def count_weights(model):
    return int(np.sum([K.count_params(p) for p in set(model.trainable_weights)]))
  
def get_l1_norms(model, layer_ix):
    # Get the weights for the layer.
    layer = model.layers[layer_ix]
    W = layer.get_weights()[0]

    # Sum up all the weights for each filter.
    l1 = np.sum(np.abs(W), axis=(0,1,2))

    # Make list of (filter_ix, l1_norm), sorted by l1_norm (low to high).
    l1_norms = sorted(list(zip(range(len(l1)), l1)), key=lambda x: x[1])
    return l1_norms

  
def plot_l1_norms(model, layer_ix):
    fig = plt.figure(figsize=(10, 5))
    plt.plot(list(map(lambda x: x[1], get_l1_norms(model, layer_ix))))
    plt.xlabel("Output channel", fontsize=18)
    plt.ylabel("L1 norm", fontsize=18)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.tick_params(axis='both', which='minor', labelsize=10)
    plt.title(model.layers[layer_ix].name, fontsize=18)





score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
print('Parameters:', count_weights(model))

print()
#for layer in model.layers:
#    print(layer.get_output_at(0).get_shape().as_list())
#print('Summary:', model.summary())
print('Summary:', model.count_params())
layer = model.layers[1]
print(layer.get_weights()[0])
plot_l1_norms(model, 1)