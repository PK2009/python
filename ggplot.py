import pandas as pd
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d 
#import Axes3D
plt.style.use('ggplot')
sread = pd.read_csv('/Users/prathmesh/Desktop/ML/students.data', index_col=0) 
sread.plot.scatter(x='G1',y='G3')
print (sread)
mu = sread.G3
mydataf = sread[['G3','G2','G1']]
mu.plot.hist(normed=True)
mydataf.plot.hist(alpha=0.5)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlabel('Final Grade')
ax.set_ylabel('First Grade')
ax.set_zlabel('Daily Alcohol')
ax.scatter(sread.G1,sread.G3,sread['Dalc'], c='r', marker = '.')
plt.show()